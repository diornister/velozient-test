Used VS Code for coding.

Dockerfile and docker-compose are configured so it will run on any environment

There was no need to add any external package.

There was no need to use a variation of Dijkstra's algorithm.

A more elegant solution was added to the data input:
a input folder was created and imported in the src/index.js script
the data format is the same as the example, but joined as one object:
eg:

interface DronesI {
name: string
maxW: number
}

interface PackagesI {
name: string
pkgW: number
}

interface DataI {
drones: DronesI[],
packages: PackagesI[]
}

To run just execute on terminal
npm start or docker-compose up
