import data from "../input/index.js"

// Generic order numeric list function basead on wProp
const orderList = (data, wProp) => {
  return data.sort((a, b) => b[`${wProp}`] - a[`${wProp}`])
}

// display the information on terminal
const dispatchDrones = (readyDrones) => {
  Object.keys(readyDrones)
    .sort((a, b) => a.localeCompare(b))
    .map((key) => {
      console.log(`-----${key}-----`)
      Object.keys(readyDrones[key]).map((locationKey) => {
        console.log(locationKey)
        readyDrones[key][locationKey].map((location) => {
          console.log(location.name)
        })
      })
      console.log("\n")
    })
}

// add package to selected drone and remove package from packages as needed
const getPackagesForDrone = (drone, packages) => {
  const dronePkgs = []
  let total = 0

  while (packages.length && total + packages[0].pkgW <= drone.maxW) {
    const pkg = packages.shift()
    total += pkg.pkgW
    dronePkgs.push(pkg)
  }

  return {
    droneData: { drone, packages: dronePkgs },
    resPkgs: packages,
  }
}

// validate if all packages can be delivered
const weightValidation = (maxDroneW, maxPkgW) => {
  if (maxDroneW < maxPkgW) {
    console.log(
      "There is a package with more weight then any drone can handle "
    )
    process.exit(1)
  }
}

// prepare drone dispatch info for display
const orderDroneInfo = (readyDrones) => {
  const droneInfoList = {}
  readyDrones.map(({ drone }) => (droneInfoList[drone.name] = {}))
  readyDrones.map(({ drone, trip, packages }) => {
    droneInfoList[drone.name][trip] = packages
  })

  return droneInfoList
}

// Main logic
// sort drones and packages by desc order
// control number of trips
const exec = () => {
  const { packages, drones } = data
  if (drones.length >= 100) return "Max number of Drones should be 100"

  const sortedDrones = orderList(drones, "maxW")
  let sortedPackages = orderList(packages, "pkgW")
  weightValidation(sortedDrones[0].maxW, sortedPackages[0].pkgW)

  let trip = 1
  let readyDrones = []
  while (sortedPackages.length) {
    for (let i = 0; i < sortedDrones.length; i++) {
      const res = getPackagesForDrone(sortedDrones[i], sortedPackages)
      sortedPackages = res.resPkgs
      readyDrones.push({ ...res.droneData, trip: `Trip #${trip}` })
    }
    trip++
  }

  dispatchDrones(orderDroneInfo(readyDrones))
}

exec()
