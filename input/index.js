const data = {
  drones: [
    {
      name: "drone 1",
      maxW: 10,
    },
    {
      name: "drone 2",
      maxW: 15,
    },
    {
      name: "drone 3",
      maxW: 20,
    },
  ],
  packages: [
    {
      name: "location 1",
      pkgW: 15,
    },
    {
      name: "location 2",
      pkgW: 10,
    },
    {
      name: "location 3",
      pkgW: 4,
    },
    {
      name: "location 4",
      pkgW: 5,
    },
    {
      name: "location 5",
      pkgW: 8,
    },
    {
      name: "location 6",
      pkgW: 7,
    },
    {
      name: "location 7",
      pkgW: 11,
    },
    {
      name: "location 8",
      pkgW: 5,
    },
    {
      name: "location 9",
      pkgW: 2,
    },
    {
      name: "location 10",
      pkgW: 6,
    },
  ],
}

export default data
