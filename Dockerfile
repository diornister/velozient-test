FROM node:14.18-alpine
WORKDIR /app

COPY ./ ./

CMD node src/index.js
